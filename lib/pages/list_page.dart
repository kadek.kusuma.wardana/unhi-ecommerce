import 'dart:convert';
import 'package:commerce/pages/pages.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:commerce/model/shop.dart';

class ListPage extends StatefulWidget {
  const ListPage({super.key});

  @override
  State<ListPage> createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  late Future<List<Shop>> data;

  Future<List<Shop>> fetchData() async {
    var url = 'https://kusumawardanastudio.com/api/api_online.php';

    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      Map api = json.decode(response.body);
      // print('data api');
      // print(api['data']);
      List jsonResponse = api['data'];

      return jsonResponse.map((data) => Shop.fromJson(data)).toList();
    } else {
      throw Exception('gagal mendapatkan data API');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    data = fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        foregroundColor: Colors.white,
        title: Text('UNHI Shop'),
      ),
      body: FutureBuilder<List<Shop>>(
        future: data,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data!.length,
              itemBuilder: (context, index) {
                var shop = snapshot.data![index];
                return ListTile(
                  onTap: () {
                    Shop model = Shop(
                      id: shop.id,
                      title: shop.title,
                      description: shop.description,
                      image: shop.image,
                      harga: shop.harga,
                    );

                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => DetailPage(model: model),
                      ),
                    );
                  },
                  title: Text(
                    '${shop.title}',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Text(
                    '${shop.description}',
                    maxLines: 2,
                    style: TextStyle(overflow: TextOverflow.ellipsis),
                  ),
                  leading: Image.network(shop.image),
                  trailing: Text('${shop.harga}'),
                );
              },
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => CreatePage(),
            ),
          );
        },
        backgroundColor: Colors.amberAccent,
        child: Icon(Icons.add),
      ),
    );
  }
}
