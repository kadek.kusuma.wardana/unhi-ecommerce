import 'package:flutter/material.dart';
import 'package:commerce/model/shop.dart';

class DetailPage extends StatelessWidget {
  // const DetailPage({Key? key}) : super(key: key,);
  const DetailPage({super.key, required this.model});

  final Shop model;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        foregroundColor: Colors.white,
        title: Text('Detail'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.network(model.image),
            SizedBox(
              height: 20,
            ),
            Text(
              '${model.title}',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(
              '${model.harga}',
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              '${model.description}',
            )
          ],
        ),
      ),
    );
  }
}
