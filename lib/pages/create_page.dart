import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CreatePage extends StatefulWidget {
  const CreatePage({Key? key}) : super(key: key);

  @override
  _CreatePageState createState() => _CreatePageState();
}

class _CreatePageState extends State<CreatePage> {
  final title = TextEditingController();
  final description = TextEditingController();
  final harga = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  void sendData() async {
    var url = 'https://kusumawardanastudio.com/api/api_create.php';

    var dataSend = {
      'title': title.text,
      'description': description.text,
      'harga': harga.text
    };
    final response = await http.post(
      Uri.parse(url),
      body: dataSend,
    );
    if (response.statusCode == 200) {
      Map api = json.decode(response.body);
      print(api['status']);
      if (api['status'] == 'Success') {
        final snackBar = SnackBar(
          content: Text(api['message']),
          action: SnackBarAction(
            label: 'Undo',
            textColor: Colors.green,
            onPressed: () {
              // Some code to undo the change.
            },
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      } else if (api['status'] == 'Error') {
        final snackBar = SnackBar(
          content: Text(api['message']),
          action: SnackBarAction(
            label: 'Undo',
            textColor: Colors.red,
            onPressed: () {
              // Some code to undo the change.
            },
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      } else {
        final snackBar = SnackBar(
          content: Text('Tidak diketahui'),
          action: SnackBarAction(
            label: 'Undo',
            textColor: Colors.yellow,
            onPressed: () {
              // Some code to undo the change.
            },
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    } else {
      throw Exception('gagal untuk dapat data');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        foregroundColor: Colors.white,
        title: Text('Tambah Data'),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                controller: title,
                decoration: const InputDecoration(
                  labelText: 'Title',
                ),
                onSaved: (String? value) {
                  // This optional block of code can be used to run
                  // code when the user saves the form.
                },
                validator: (String? value) {
                  return (value != null && value.contains('@'))
                      ? 'Do not use the @ char.'
                      : null;
                },
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: description,
                decoration: const InputDecoration(
                  labelText: 'Description',
                ),
                onSaved: (String? value) {
                  // This optional block of code can be used to run
                  // code when the user saves the form.
                },
                validator: (String? value) {
                  return (value != null && value.contains('@'))
                      ? 'Do not use the @ char.'
                      : null;
                },
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                keyboardType: TextInputType.number,
                controller: harga,
                decoration: const InputDecoration(
                  labelText: 'harga',
                ),
                onSaved: (String? value) {
                  // This optional block of code can be used to run
                  // code when the user saves the form.
                },
                validator: (String? value) {
                  return (value != null && value.contains('@'))
                      ? 'Do not use the @ char.'
                      : null;
                },
              ),
              FilledButton(
                onPressed: () {
                  sendData();
                },
                child: Text('Submit'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
